package com.cisco.merakinetworks;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.NavigationUI;

import com.cisco.merakinetworks.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity {

    private ActivityMainBinding bindings = null;

    private NavController navController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bindings = DataBindingUtil.setContentView(this, R.layout.activity_main);

        navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        navController.addOnDestinationChangedListener(((controller, destination, arguments) -> {
            if (destination.getId() == R.id.fragmentOnline ||
                    destination.getId() == R.id.fragmentOffline) {
                bindings.setHideBottomNavigation(false);
            } else {
                bindings.setHideBottomNavigation(true);
            }
        }));

        NavigationUI.setupWithNavController(bindings.bottomTabs, navController);

        bindings.bottomTabs.setItemIconTintList(null);
    }
}
