package com.cisco.merakinetworks.core.network;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkInfo;
import android.net.NetworkRequest;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.Observable;

/**
 * Implementing {@link ConnectivityRepo}
 */
public class ConnectivityRepoImpl extends Observable implements ConnectivityRepo {

    /**
     * Callback that actually monitors changes to the network
     */
    private ConnectivityManager.NetworkCallback networkCallback = new ConnectivityManager.NetworkCallback() {
        @Override
        public void onAvailable(@NonNull Network network) {
            super.onAvailable(network);
            notifyAllObservers(true);
        }

        @Override
        public void onLosing(@NonNull Network network, int maxMsToLive) {
            super.onLosing(network, maxMsToLive);
            notifyAllObservers(false);
        }

        @Override
        public void onLost(@NonNull Network network) {
            super.onLost(network);
            notifyAllObservers(false);
        }

        @Override
        public void onUnavailable() {
            super.onUnavailable();
            notifyAllObservers(false);
        }
    };

    /**
     * Live data stream to notify objects that are observing via viewModels
     */
    private MutableLiveData<Boolean> isConnectedLiveData = new MutableLiveData<>();

    private ConnectivityManager connectivityManager;

    public ConnectivityRepoImpl(Context context, ConnectivityManager connectivityManager) {

        this.connectivityManager = connectivityManager;

        // Update your internal state as quickly as possible. Network callbacks are not called
        // immediately
        notifyAllObservers(isNetworkConnected());

        this.connectivityManager.registerNetworkCallback(new NetworkRequest.Builder()
                .addTransportType(NetworkCapabilities.TRANSPORT_CELLULAR)
                .addTransportType(NetworkCapabilities.TRANSPORT_WIFI)
                .build(), networkCallback);


    }

    /**
     * Returns a Live data that can be transformed
     * @return
     */
    @Override
    public LiveData<Boolean> isConnected() {
        return isConnectedLiveData;
    }

    /**
     * Helper function to perform 2 things in one go.
     * Update the LiveData transforms and direct observers
     * @param result
     */
    private void notifyAllObservers(boolean result) {

        setChanged();
        notifyObservers(result);

        if (isConnectedLiveData.getValue() == null) {
            isConnectedLiveData.setValue(result);
        } else if (isConnectedLiveData.getValue() != result) {
            isConnectedLiveData.postValue(result);
        }
    }

    /**
     * Wrapper function to test if network is connected or not
     * @return
     */
    private boolean isNetworkConnected()  {
        NetworkInfo activeNetwork = connectivityManager.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnected();
    }
}
