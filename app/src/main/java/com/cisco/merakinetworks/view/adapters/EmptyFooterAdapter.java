package com.cisco.merakinetworks.view.adapters;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.cisco.merakinetworks.R;
import com.cisco.merakinetworks.databinding.EmptyFooterViewHolderBinding;
import com.cisco.merakinetworks.view.viewHolders.FooterViewHolder;

/**
 * Simple empty footer to make sure the list items don't overlap with bottom edge of screen
 */
public class EmptyFooterAdapter extends RecyclerView.Adapter<FooterViewHolder> {

    @NonNull
    @Override
    public FooterViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        return new FooterViewHolder(EmptyFooterViewHolderBinding.inflate(inflater, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull FooterViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 1;
    }

    @Override
    public int getItemViewType(int position) {
        return R.layout.empty_footer_view_holder;
    }
}
