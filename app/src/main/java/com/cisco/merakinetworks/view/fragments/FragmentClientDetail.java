package com.cisco.merakinetworks.view.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.MergeAdapter;

import com.cisco.merakinetworks.databinding.FragmentClientDetailBinding;
import com.cisco.merakinetworks.middleware.model.Client;
import com.cisco.merakinetworks.view.adapters.ClientDetailAdapter;
import com.cisco.merakinetworks.view.adapters.ClientDetailHeaderAdapter;
import com.cisco.merakinetworks.view.adapters.EmptyFooterAdapter;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentClientDetail extends Fragment {

    private FragmentClientDetailBinding binding;

    public FragmentClientDetail() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentClientDetailBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        binding.btnBack.setOnClickListener((v) -> Navigation.findNavController(requireView()).navigateUp());

        Client client = FragmentClientDetailArgs.fromBundle(requireArguments()).getClient();

        binding.clientDetailList.setAdapter(new MergeAdapter(new ClientDetailHeaderAdapter(client),
                new ClientDetailAdapter(requireContext(), client.getRawTraffic()),
                new EmptyFooterAdapter()));
    }
}
