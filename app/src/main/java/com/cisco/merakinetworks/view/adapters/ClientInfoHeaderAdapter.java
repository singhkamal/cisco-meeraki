package com.cisco.merakinetworks.view.adapters;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.cisco.merakinetworks.R;
import com.cisco.merakinetworks.databinding.ClientInfoHeaderViewHolderBinding;
import com.cisco.merakinetworks.view.viewHolders.ClientsInfoHeaderViewHolder;

/**
 * Renders the total number of clients in a given section
 */
public class ClientInfoHeaderAdapter extends RecyclerView.Adapter<ClientsInfoHeaderViewHolder> {

    private boolean online;

    private int totalDevices = 0;

    public ClientInfoHeaderAdapter(boolean online, int totalDevices) {
        this.online = online;
        this.totalDevices = totalDevices;
    }

    @NonNull
    @Override
    public ClientsInfoHeaderViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        return new ClientsInfoHeaderViewHolder(ClientInfoHeaderViewHolderBinding.inflate(inflater, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ClientsInfoHeaderViewHolder holder, int position) {
        holder.bind(totalDevices, online);
    }

    @Override
    public int getItemCount() {
        return 1;
    }

    @Override
    public int getItemViewType(int position) {
        return R.layout.client_info_header_view_holder;
    }
}
