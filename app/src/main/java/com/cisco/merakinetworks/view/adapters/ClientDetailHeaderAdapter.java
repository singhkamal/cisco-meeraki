package com.cisco.merakinetworks.view.adapters;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.cisco.merakinetworks.databinding.ClientDetailHeaderViewHolderBinding;
import com.cisco.merakinetworks.middleware.model.Client;
import com.cisco.merakinetworks.view.viewHolders.ClientDetailHeaderViewHolder;

/**
 * Renders the client's information in a header
 */
public class ClientDetailHeaderAdapter extends RecyclerView.Adapter<ClientDetailHeaderViewHolder> {

    private Client client;

    public ClientDetailHeaderAdapter(Client client) {
        this.client = client;
    }

    @NonNull
    @Override
    public ClientDetailHeaderViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        return new ClientDetailHeaderViewHolder(ClientDetailHeaderViewHolderBinding.inflate(inflater, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ClientDetailHeaderViewHolder holder, int position) {
        holder.bind(client);
    }

    @Override
    public int getItemCount() {
        return 1;
    }
}
