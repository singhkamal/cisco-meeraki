package com.cisco.merakinetworks.view.fragments;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.MergeAdapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.cisco.merakinetworks.databinding.FragmentOfflineBinding;
import com.cisco.merakinetworks.middleware.model.Client;
import com.cisco.merakinetworks.middleware.viewmodels.MerakiClientsViewModel;
import com.cisco.merakinetworks.view.adapters.ClientInfoHeaderAdapter;
import com.cisco.merakinetworks.view.adapters.ClientsInfoAdapter;
import com.cisco.merakinetworks.view.adapters.EmptyFooterAdapter;
import com.cisco.merakinetworks.view.viewHolders.ClientClickedObserver;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentOffline extends Fragment implements ClientClickedObserver {

    private FragmentOfflineBinding binding;

    public FragmentOffline() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentOfflineBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        MerakiClientsViewModel viewModel = new ViewModelProvider(this, new MerakiClientsViewModel.Factory()).get(MerakiClientsViewModel.class);
        viewModel.getOfflineMerakiClients().observeForever(merakiClients -> {

            if (merakiClients != null) {
                binding.setClientsLoaded(true);
                binding.setErrorFetchingClients(false);

                binding.clientList.setAdapter(new MergeAdapter(new ClientInfoHeaderAdapter(false, merakiClients.size()),
                        new ClientsInfoAdapter(merakiClients, this),
                        new EmptyFooterAdapter()));

            } else {
                binding.setClientsLoaded(false);
                binding.setErrorFetchingClients(true);
            }
        });
    }

    @Override
    public void onClientClicked(Client client) {
        Navigation.findNavController(requireView()).navigate(FragmentOfflineDirections.actionFragmentOfflineToFragmentClientDetail(client));
    }
}
