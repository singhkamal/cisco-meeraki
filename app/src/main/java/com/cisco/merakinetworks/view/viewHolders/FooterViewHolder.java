package com.cisco.merakinetworks.view.viewHolders;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.cisco.merakinetworks.databinding.EmptyFooterViewHolderBinding;

public class FooterViewHolder extends RecyclerView.ViewHolder {

    private EmptyFooterViewHolderBinding binding;

    public FooterViewHolder(@NonNull EmptyFooterViewHolderBinding binding) {
        super(binding.getRoot());
        this.binding = binding;
    }
}
