package com.cisco.merakinetworks.view.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.MergeAdapter;

import com.cisco.merakinetworks.databinding.FragmentOnlineBinding;
import com.cisco.merakinetworks.middleware.model.Client;
import com.cisco.merakinetworks.middleware.viewmodels.ConnectivityViewModel;
import com.cisco.merakinetworks.middleware.viewmodels.MerakiClientsViewModel;
import com.cisco.merakinetworks.utils.EspressoIdling;
import com.cisco.merakinetworks.view.adapters.ClientInfoHeaderAdapter;
import com.cisco.merakinetworks.view.adapters.ClientsInfoAdapter;
import com.cisco.merakinetworks.view.adapters.EmptyFooterAdapter;
import com.cisco.merakinetworks.view.viewHolders.ClientClickedObserver;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentOnline extends Fragment implements ClientClickedObserver {

    FragmentOnlineBinding binding;

    public FragmentOnline() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentOnlineBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ConnectivityViewModel connectivityViewModel = new ViewModelProvider(this, new ConnectivityViewModel.Factory()).get(ConnectivityViewModel.class);
        connectivityViewModel.isConnected().observe(getViewLifecycleOwner(), connected -> {
            if (!connected) {
                Navigation.findNavController(requireView()).navigate(FragmentOnlineDirections.actionFragmentOnlineToFragmentNoConnectivity());
            }
        });

        /**
         EspressoIdling.INSTANCE.increment(); is used only to assist the espresso testing.
         For more information on this, please check androidTest FragmentOnlineTest and {@link EspressoIdling}
         */
        EspressoIdling.INSTANCE.increment();

        MerakiClientsViewModel viewModel = new ViewModelProvider(this, new MerakiClientsViewModel.Factory()).get(MerakiClientsViewModel.class);

        viewModel.getOnlineMerakiClients().observe(getViewLifecycleOwner(), merakiClients -> {

            if (merakiClients != null) {
                binding.setClientsLoaded(true);
                binding.setErrorFetchingClients(false);

                binding.clientList.setAdapter(new MergeAdapter(new ClientInfoHeaderAdapter(true, merakiClients.size()),
                        new ClientsInfoAdapter(merakiClients, this),
                        new EmptyFooterAdapter()));

            } else {
                binding.setClientsLoaded(false);
                binding.setErrorFetchingClients(true);
            }
            EspressoIdling.INSTANCE.decrement();
        });
    }

    @Override
    public void onClientClicked(Client client) {
        Navigation.findNavController(requireView()).navigate(FragmentOnlineDirections.actionFragmentOnlineToFragmentClientDetail(client));
    }
}
