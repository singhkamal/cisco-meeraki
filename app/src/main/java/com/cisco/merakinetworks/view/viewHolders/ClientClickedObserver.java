package com.cisco.merakinetworks.view.viewHolders;

import com.cisco.merakinetworks.middleware.model.Client;

/**
 * Used to launch {@link com.cisco.merakinetworks.view.fragments.FragmentClientDetail} when item is clicked either on
 * {@link com.cisco.merakinetworks.view.fragments.FragmentOnline} and {@link com.cisco.merakinetworks.view.fragments.FragmentOffline}
 */
public interface ClientClickedObserver {

    void onClientClicked(Client client);
}
