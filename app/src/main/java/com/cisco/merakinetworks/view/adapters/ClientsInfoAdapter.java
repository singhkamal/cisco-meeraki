package com.cisco.merakinetworks.view.adapters;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.cisco.merakinetworks.R;
import com.cisco.merakinetworks.databinding.ClientInfoViewHolderBinding;
import com.cisco.merakinetworks.middleware.model.Client;
import com.cisco.merakinetworks.view.viewHolders.ClientClickedObserver;
import com.cisco.merakinetworks.view.viewHolders.ClientInfoViewHolder;

import java.util.List;

/**
 * Renders basic client's information like name, ip address and last seen status
 */
public class ClientsInfoAdapter extends RecyclerView.Adapter<ClientInfoViewHolder> {

    private List<Client> clients;

    private ClientClickedObserver clientClickedObserver;

    public ClientsInfoAdapter(List<Client> clients, ClientClickedObserver clientClickedObserver) {
        this.clients = clients;
        this.clientClickedObserver = clientClickedObserver;
    }

    @NonNull
    @Override
    public ClientInfoViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        return new ClientInfoViewHolder(ClientInfoViewHolderBinding.inflate(inflater, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ClientInfoViewHolder holder, int position) {
        holder.bind(clients.get(position), clientClickedObserver);
    }

    @Override
    public int getItemCount() {
        return clients.size();
    }

    @Override
    public int getItemViewType(int position) {
        return R.layout.client_info_view_holder;
    }
}
