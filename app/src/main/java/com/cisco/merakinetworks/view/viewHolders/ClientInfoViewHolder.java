package com.cisco.merakinetworks.view.viewHolders;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.cisco.merakinetworks.R;
import com.cisco.merakinetworks.databinding.ClientInfoViewHolderBinding;
import com.cisco.merakinetworks.middleware.model.Client;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class ClientInfoViewHolder extends RecyclerView.ViewHolder {

    private ClientInfoViewHolderBinding binding;

    public ClientInfoViewHolder(@NonNull ClientInfoViewHolderBinding binding) {
        super(binding.getRoot());
        this.binding = binding;
    }

    public void bind(Client client, ClientClickedObserver clientClickedObserver) {

        Context context = binding.title.getContext();

        binding.setClient(client);

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss");
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis() - client.getOnlineStatus().getLastSeen());


        binding.onlineStatus.setText(String.format(context.getString(R.string.last_seen), sdf.format(calendar.getTime())));

        binding.setClientClickObserver(clientClickedObserver);
    }
}
