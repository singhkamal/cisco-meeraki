package com.cisco.merakinetworks.view.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;

import com.cisco.merakinetworks.databinding.FragmentNoConnectivityBinding;
import com.cisco.merakinetworks.middleware.viewmodels.ConnectivityViewModel;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentNoConnectivity extends Fragment {

    public FragmentNoConnectivity() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        FragmentNoConnectivityBinding binding = FragmentNoConnectivityBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ConnectivityViewModel connectivityViewModel = new ViewModelProvider(this, new ConnectivityViewModel.Factory()).get(ConnectivityViewModel.class);
        /**
         * If we are connected, go back to home
         */
        connectivityViewModel.isConnected().observe(getViewLifecycleOwner(), connected -> {
            if (connected) {
                Navigation.findNavController(requireView()).navigate(FragmentNoConnectivityDirections.actionFragmentNoConnectivityToFragmentOnline());
            }
        });
    }
}
