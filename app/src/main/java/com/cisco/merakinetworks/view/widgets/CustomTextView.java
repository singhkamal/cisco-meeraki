package com.cisco.merakinetworks.view.widgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatTextView;

import com.cisco.merakinetworks.R;


/**
 * Custom Text view to apply application specific font to all text elements
 */
public class CustomTextView extends AppCompatTextView {
    public CustomTextView(Context context) {
        this(context, null);
    }

    public CustomTextView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CustomTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        if (attrs != null) {
            TypedArray array = context.obtainStyledAttributes(attrs, R.styleable.CustomText);

            setTypeface(context, array.getString(R.styleable.CustomText_typeface));

            array.recycle();
        }
    }

    public boolean setTypeface(Context context, String asset) {
        if (asset == null) return false;

        Typeface tf = getTypeface(context, asset);

        if (isInEditMode() && tf == null) {
            return false;
        }

        setTypeface(tf);

        return true;
    }

    private Typeface getTypeface(Context context, String typeface) {
        if (typeface != null && context != null) {
            try {
                return Typeface.createFromAsset(context.getAssets(), typeface);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }
}
