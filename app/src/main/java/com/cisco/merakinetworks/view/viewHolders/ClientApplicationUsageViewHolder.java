package com.cisco.merakinetworks.view.viewHolders;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.cisco.merakinetworks.databinding.ClientApplicationUsageViewHolderBinding;

public class ClientApplicationUsageViewHolder extends RecyclerView.ViewHolder {

    private ClientApplicationUsageViewHolderBinding binding;

    public ClientApplicationUsageViewHolder(@NonNull ClientApplicationUsageViewHolderBinding binding) {
        super(binding.getRoot());
        this.binding = binding;
    }

    public void bind(String label) {
        binding.setLabel(label);
    }

    public void bind(String applicationName, int progress, int maxProgress) {
        binding.setApplicationName(applicationName);
        binding.usageProgressBar.setMax(maxProgress);
        binding.usageProgressBar.setProgress(progress);
    }
}
