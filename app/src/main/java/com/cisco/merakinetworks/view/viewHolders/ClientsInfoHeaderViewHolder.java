package com.cisco.merakinetworks.view.viewHolders;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.cisco.merakinetworks.R;
import com.cisco.merakinetworks.databinding.ClientInfoHeaderViewHolderBinding;

public class ClientsInfoHeaderViewHolder extends RecyclerView.ViewHolder {

    private ClientInfoHeaderViewHolderBinding binding;

    public ClientsInfoHeaderViewHolder(@NonNull ClientInfoHeaderViewHolderBinding binding) {
        super(binding.getRoot());
        this.binding = binding;
    }

    public void bind(int numberOfClients, boolean online) {

        Context context = binding.title.getContext();

        if (online) {
            binding.title.setText(String.format(context.getString(R.string.online_clients), numberOfClients));
        } else {
            binding.title.setText(String.format(context.getString(R.string.offline_clients), numberOfClients));
        }
    }
}
