package com.cisco.merakinetworks.view.viewHolders;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.cisco.merakinetworks.R;
import com.cisco.merakinetworks.databinding.ClientDetailHeaderViewHolderBinding;
import com.cisco.merakinetworks.middleware.model.Client;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class ClientDetailHeaderViewHolder extends RecyclerView.ViewHolder {

    private ClientDetailHeaderViewHolderBinding binding;

    public ClientDetailHeaderViewHolder(@NonNull ClientDetailHeaderViewHolderBinding binding) {
        super(binding.getRoot());
        this.binding = binding;
    }

    public void bind(Client client) {
        binding.setClient(client);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss", Locale.getDefault());
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis() - client.getOnlineStatus().getLastSeen());


        binding.lastSeen.setText(String.format(binding.lastSeen.getContext().getString(R.string.last_seen), sdf.format(calendar.getTime())));

        Float totalBytesSent = client.getRawTraffic().getSent() + client.getRawTraffic().getRecv();

        int sendProgress = Float.valueOf((client.getRawTraffic().getSent() / totalBytesSent) * 100.0f).intValue();
        int recvProgress = Float.valueOf((client.getRawTraffic().getRecv() / totalBytesSent) * 100.0f).intValue();

        binding.sendProgressBar.setProgress(sendProgress);
        binding.recvProgressBar.setProgress(recvProgress);
    }
}
