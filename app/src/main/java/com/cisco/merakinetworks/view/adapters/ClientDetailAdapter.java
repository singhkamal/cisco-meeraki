package com.cisco.merakinetworks.view.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.cisco.merakinetworks.R;
import com.cisco.merakinetworks.databinding.ClientApplicationUsageViewHolderBinding;
import com.cisco.merakinetworks.middleware.model.ApplicationUsage;
import com.cisco.merakinetworks.middleware.model.RawTraffic;
import com.cisco.merakinetworks.view.viewHolders.ClientApplicationUsageViewHolder;

import java.util.List;

/**
 * Adapter to render client detailed application usage information
 */
public class ClientDetailAdapter extends RecyclerView.Adapter<ClientApplicationUsageViewHolder> {

    private RawTraffic rawTraffic;

    private List<ApplicationUsage> applicationUsages;

    private String headerLabel = "";

    public ClientDetailAdapter(Context context, RawTraffic rawTraffic) {
        this.rawTraffic = rawTraffic;
        applicationUsages = rawTraffic.getApplicationUsage();
        headerLabel = context.getString(R.string.application_usage);
    }

    @NonNull
    @Override
    public ClientApplicationUsageViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        return new ClientApplicationUsageViewHolder(ClientApplicationUsageViewHolderBinding.inflate(inflater, parent, false));
    }

    /**
     * from documentation, we cannot just add usage of all application as there may be some data from unknown origin.
     * Hence I am adding sent/received and then checking it's usage again application's usage
     * @param holder
     * @param position
     */
    @Override
    public void onBindViewHolder(@NonNull ClientApplicationUsageViewHolder holder, int position) {
        if (position == 0) {
            holder.bind(headerLabel);
        } else {
            int totalBytesSent = Float.valueOf(rawTraffic.getSent() + rawTraffic.getRecv()).intValue();

            ApplicationUsage applicationUsage = applicationUsages.get(position - 1);

            holder.bind(applicationUsage.applicationName, applicationUsage.usage, totalBytesSent);
        }
    }

    @Override
    public int getItemCount() {
        return applicationUsages.size() + 1; // +1 is for header
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return R.layout.label_view_holder;
        }
        return R.layout.client_application_usage_view_holder;
    }
}
