package com.cisco.merakinetworks;

/**
 * This allows construction injection.
 * It also helps in mocking in unit testing where we can provide a custom instance of
 * {@link DependencyResolver}
 */
public class DependencyProvider {

    public static DependencyProvider INSTANCE;

    private DependencyResolver resolver;

    public DependencyProvider(MainApplication application) {
        resolver = DaggerDependencyResolver.builder()
                .dependencyFactory(new DependencyFactory(application))
                .build();
        INSTANCE = this;
    }

    public DependencyResolver getResolver() {
        return resolver;
    }

    /**
     * Custom API required to help Unit Testing
     * @param resolver
     */
    public void setResolver(DependencyResolver resolver) {
        this.resolver = resolver;
    }
}
