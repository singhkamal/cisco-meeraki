package com.cisco.merakinetworks;

import com.cisco.merakinetworks.middleware.viewmodels.ConnectivityViewModel;
import com.cisco.merakinetworks.middleware.viewmodels.MerakiClientsViewModel;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Dagger dependency resolver
 */
@Singleton
@Component(modules = {DependencyFactory.class})
public interface DependencyResolver {

    void inject(ConnectivityViewModel connectivityViewModel);

    void inject(MerakiClientsViewModel merakiClientsViewModel);
}
