package com.cisco.merakinetworks;

import android.app.Application;
import android.content.Context;
import android.net.ConnectivityManager;


import com.cisco.merakinetworks.core.network.ConnectivityRepo;
import com.cisco.merakinetworks.core.network.ConnectivityRepoImpl;
import com.cisco.merakinetworks.middleware.model.MerakiClients;
import com.cisco.merakinetworks.middleware.net.MerakiApi;
import com.cisco.merakinetworks.middleware.repositories.MerakiClientsRepository;
import com.cisco.merakinetworks.middleware.repositories.MerakiClientsRepositoryImpl;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Dagger enabled dependency factory
 *
 */
@Module
public class DependencyFactory {
    private Application application;

    public DependencyFactory(Application application) {
        this.application = application;
    }

    @Provides
    Context getContext() {
        return application.getApplicationContext();
    }


    @Provides
    @Singleton
    ConnectivityManager getConnectivityManager() {
        return (ConnectivityManager) application.getSystemService(Context.CONNECTIVITY_SERVICE);
    }

    @Provides
    @Singleton
    ConnectivityRepo getConnectivityRepo(Context context, ConnectivityManager connectivityManager) {
        return new ConnectivityRepoImpl(context, connectivityManager);
    }

    @Provides
    @Singleton
    HttpLoggingInterceptor getHttpLoggingInterceptor() {
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(BuildConfig.DEBUG ? HttpLoggingInterceptor.Level.BODY : HttpLoggingInterceptor.Level.NONE);
        return loggingInterceptor;
    }

    @Provides
    @Singleton
    OkHttpClient okHttpClient(HttpLoggingInterceptor httpLoggingInterceptor) {
        return new OkHttpClient.Builder()
                .addInterceptor(httpLoggingInterceptor)
                .build();
    }

    /**
     * Due to variable entries in {@link com.cisco.merakinetworks.middleware.model.RawTraffic}'s Application usage object
     * we have to do custom JSON parsing
     * @return
     */
    @Provides
    @Singleton
    Gson getGson() {
        return new GsonBuilder()
                .registerTypeAdapter(MerakiClients.class, MerakiClients.getDeserializer())
                .create();
    }

    @Provides
    @Singleton
    Retrofit getRetrofit(OkHttpClient okHttpClient, Gson gson) {
        return new Retrofit.Builder()
                .client(okHttpClient)
                .baseUrl(BuildConfig.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
    }

    @Provides
    @Singleton
    MerakiApi getMerakiAPI(Retrofit retrofit) {
        return retrofit.create(MerakiApi.class);
    }

    @Provides
    @Singleton
    MerakiClientsRepository getMerakiClientsRepository(ConnectivityRepo connectivityRepo, MerakiApi merakiApi) {
        return new MerakiClientsRepositoryImpl(connectivityRepo, merakiApi);
    }
}
