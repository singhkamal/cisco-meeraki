package com.cisco.merakinetworks.middleware.model;

/**
 * <pre>
 * Application usage item representing a single item from object
 *
 * applicationUsage: {
 *      DNS: 19357,
 *      Google HTTPS: 89427,
 *      Google: 4,
 *      YouTube: 11073164,
 *      Facebook: 367,
 *      Netflix: 7623989,
 *      CNN: 1007,
 *      amazon.com: 224400,
 *      CDNs: 983890,
 *      Miscellaneous video: 2793182,
 *      Miscellaneous web: 2371545,
 *      Miscellaneous secure web: 754630,
 *      ICMP: 85,
 *      Non-web TCP: 51820,
 *      UDP: 211611,
 *      Other: 1,
 *      turner.com: 1807,
 *      Miscellaneous audio: 43,
 *      Google Drive: 6264,
 *      Amazon Instant Video: 234212
 * },
 * </pre>
 */
public class ApplicationUsage implements Comparable<ApplicationUsage> {

    public final String applicationName;

    public final Integer usage;

    public ApplicationUsage(String applicationName, Integer usage) {
        this.applicationName = applicationName;
        this.usage = usage;
    }

    @Override
    public int compareTo(ApplicationUsage o) {
        return usage - o.usage;
    }
}
