package com.cisco.merakinetworks.middleware.repositories;

import androidx.lifecycle.LiveData;

import com.cisco.merakinetworks.middleware.model.Client;

import java.util.List;

/**
 * Interface to get Meraki online and offline clients
 */
public interface MerakiClientsRepository {

    /**
     * Get Online Clients
     * @return
     */
    LiveData<List<Client>> getOnlineMerakiClients();

    /**
     * Get Offline Clients
     * @return
     */
    LiveData<List<Client>> getOfflineMerakiClients();

}
