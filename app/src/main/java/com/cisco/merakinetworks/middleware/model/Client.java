package com.cisco.merakinetworks.middleware.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * <pre>
 * Representing the client object
 *
 * "client": {
 *     "clientInfo": {@link ClientInfo},
 *     "onlineStatus": {@link OnlineStatus},
 *     "rawTraffic":" {@link RawTraffic}
 * }
 * </pre>
 */
public class Client implements Parcelable {
    @SerializedName("clientInfo")
    @Expose
    private ClientInfo clientInfo;
    @SerializedName("onlineStatus")
    @Expose
    private OnlineStatus onlineStatus;
    @SerializedName("rawTraffic")
    @Expose
    private RawTraffic rawTraffic;

    public ClientInfo getClientInfo() {
        return clientInfo;
    }

    public void setClientInfo(ClientInfo clientInfo) {
        this.clientInfo = clientInfo;
    }

    public OnlineStatus getOnlineStatus() {
        return onlineStatus;
    }

    public void setOnlineStatus(OnlineStatus onlineStatus) {
        this.onlineStatus = onlineStatus;
    }

    public RawTraffic getRawTraffic() {
        return rawTraffic;
    }

    public void setRawTraffic(RawTraffic rawTraffic) {
        this.rawTraffic = rawTraffic;
    }

    public Client() {
    }

    protected Client(Parcel in) {
        clientInfo = (ClientInfo) in.readValue(ClientInfo.class.getClassLoader());
        onlineStatus = (OnlineStatus) in.readValue(OnlineStatus.class.getClassLoader());
        rawTraffic = (RawTraffic) in.readValue(RawTraffic.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(clientInfo);
        dest.writeValue(onlineStatus);
        dest.writeValue(rawTraffic);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Client> CREATOR = new Parcelable.Creator<Client>() {
        @Override
        public Client createFromParcel(Parcel in) {
            return new Client(in);
        }

        @Override
        public Client[] newArray(int size) {
            return new Client[size];
        }
    };

    public static Client fromJson(JsonObject jsonObject) {
        Client client = new Client();
        client.setClientInfo(new Gson().fromJson(jsonObject.get("clientInfo"), ClientInfo.class));
        client.setOnlineStatus(new Gson().fromJson(jsonObject.get("onlineStatus"), OnlineStatus.class));
        client.setRawTraffic(RawTraffic.fromJson(jsonObject.get("rawTraffic").getAsJsonObject()));
        return client;
    }
}