package com.cisco.merakinetworks.middleware.net;

import com.cisco.merakinetworks.middleware.model.MerakiClients;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Meraki API
 */
public interface MerakiApi {

    /**
     * Returns the array of all the clients connected to Meraki network
     * @return
     */
    @GET("clients")
    Call<MerakiClients> getMerakiClients();
}
