package com.cisco.merakinetworks.middleware.repositories;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.cisco.merakinetworks.core.network.ConnectivityRepo;
import com.cisco.merakinetworks.middleware.model.Client;
import com.cisco.merakinetworks.middleware.model.MerakiClients;
import com.cisco.merakinetworks.middleware.model.OnlineStatus;
import com.cisco.merakinetworks.middleware.net.MerakiApi;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Implementation of {@link MerakiClientsRepository}
 */
public class MerakiClientsRepositoryImpl implements MerakiClientsRepository, Observer {

    private enum State {
        CONNECTED,
        NOT_CONNECTED
    }

    private State state = State.NOT_CONNECTED;

    private ConnectivityRepo connectivityRepo;

    private MerakiApi merakiApi;

    private MutableLiveData<List<Client>> merakiOnlineClientsMutableLiveData = new MutableLiveData<>();

    private MutableLiveData<List<Client>> merakiOfflineClientsMutableLiveData = new MutableLiveData<>();

    private MerakiClients merakiClients = null;

    public MerakiClientsRepositoryImpl(ConnectivityRepo connectivityRepo, MerakiApi merakiApi) {
        this.connectivityRepo = connectivityRepo;
        this.merakiApi = merakiApi;

        /**
         * Register an observer to connectivity repo. This will enable cleanup if we lose
         * network connectivity
         */
        if (connectivityRepo instanceof Observable) {
            ((Observable)connectivityRepo).addObserver(this);
        }

        /**
         * Before this repo object is available, update network status
         */
        updateConnection(connectivityRepo);
    }

    @Override
    public void update(Observable o, Object arg) {
        if (o instanceof ConnectivityRepo) {
            state = (Boolean)arg ? State.CONNECTED : State.NOT_CONNECTED;
        }
    }

    /**
     * Get all Meraki online clients. If we already have clients then return from cache
     * @return
     */
    @Override
    public LiveData<List<Client>> getOnlineMerakiClients() {
        if (merakiOfflineClientsMutableLiveData.getValue() == null) {
            getMerakiClients();
        }
        return merakiOnlineClientsMutableLiveData;
    }

    /**
     * Get all Meraki offline clients. If we already have clients then return from cache
     * @return
     */
    @Override
    public LiveData<List<Client>> getOfflineMerakiClients() {
        return merakiOfflineClientsMutableLiveData;
    }

    /**
     * Update internal state based on internet connectivity availability
     * @param repo
     */
    private void updateConnection(ConnectivityRepo repo) {
        LiveData<Boolean> connectedLiveData = repo.isConnected();
        if (connectedLiveData != null && connectedLiveData.getValue() != null) {
            state = connectedLiveData.getValue() ? State.CONNECTED : State.NOT_CONNECTED;
        }
    }

    /**
     * Network call to fetch clients
     * @return
     */
    private MerakiClients getMerakiClients() {

        if (state == State.CONNECTED && merakiClients == null) {
            merakiApi.getMerakiClients().enqueue(new Callback<MerakiClients>() {
                @Override
                public void onResponse(Call<MerakiClients> call, Response<MerakiClients> response) {
                    if (response.isSuccessful()) {
                        merakiClients = response.body();

                        /**
                         * Simple extraction of online/offline clients in to respective live data
                         */
                        if (merakiClients != null) {
                            List<Client> clients = merakiClients.getClients();
                            if (clients != null) {

                                List<Client> onlineClients = new ArrayList<>();
                                List<Client> offlineClients = new ArrayList<>();

                                for (Client client : clients) {

                                    OnlineStatus onlineStatus = client.getOnlineStatus();

                                    if (onlineStatus != null) {
                                        if (onlineStatus.getIsOnline()) {
                                            onlineClients.add(client);
                                        } else {
                                            offlineClients.add(client);
                                        }
                                    }
                                }

                                merakiOnlineClientsMutableLiveData.postValue(onlineClients);
                                merakiOfflineClientsMutableLiveData.postValue(offlineClients);

                            } else {
                                updateOnError();
                            }
                        } else {
                            updateOnError();
                        }
                    } else {
                        updateOnError();
                    }
                }

                @Override
                public void onFailure(Call<MerakiClients> call, Throwable t) {
                    updateOnError();
                }
            });
        }
        return null;
    }

    private void updateOnError() {
        merakiOnlineClientsMutableLiveData.postValue(null);
        merakiOfflineClientsMutableLiveData.postValue(null);
        merakiClients = null;
    }
}
