package com.cisco.merakinetworks.middleware.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * <pre>
 * Representing client info object
 * "clientInfo": {
 * 		"description": "cb852d5b-9111-4211-e3a4-2aba6a99eb07",
 * 		"ip": "192.168.128.83",
 * 		"isOnline": true
 *  },
 *  </pre>
 */
public class ClientInfo implements Parcelable {
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("ip")
    @Expose
    private String ip;
    @SerializedName("isOnline")
    @Expose
    private Boolean isOnline;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public Boolean getIsOnline() {
        return isOnline;
    }

    public void setIsOnline(Boolean isOnline) {
        this.isOnline = isOnline;
    }

    protected ClientInfo(Parcel in) {
        description = in.readString();
        ip = in.readString();
        byte isOnlineVal = in.readByte();
        isOnline = isOnlineVal == 0x02 ? null : isOnlineVal != 0x00;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(description);
        dest.writeString(ip);
        if (isOnline == null) {
            dest.writeByte((byte) (0x02));
        } else {
            dest.writeByte((byte) (isOnline ? 0x01 : 0x00));
        }
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<ClientInfo> CREATOR = new Parcelable.Creator<ClientInfo>() {
        @Override
        public ClientInfo createFromParcel(Parcel in) {
            return new ClientInfo(in);
        }

        @Override
        public ClientInfo[] newArray(int size) {
            return new ClientInfo[size];
        }
    };
}