package com.cisco.merakinetworks.middleware.viewmodels;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.cisco.merakinetworks.DependencyProvider;
import com.cisco.merakinetworks.core.network.ConnectivityRepo;
import com.cisco.merakinetworks.middleware.model.Client;
import com.cisco.merakinetworks.middleware.model.MerakiClients;
import com.cisco.merakinetworks.middleware.repositories.MerakiClientsRepository;

import java.util.List;

import javax.inject.Inject;

/**
 * Viewmodel wrapping {@link MerakiClientsRepository}
 */
public class MerakiClientsViewModel extends ViewModel implements MerakiClientsRepository {

    /**
     * Factory to assist unit testing
     */
    public static final class Factory implements ViewModelProvider.Factory {

        @NonNull
        @Override
        public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
            return (T) new MerakiClientsViewModel(DependencyProvider.INSTANCE);
        }
    }

    @Inject
    MerakiClientsRepository merakiClientsRepository;

    @Inject
    ConnectivityRepo connectivityRepo;

    public MerakiClientsViewModel(DependencyProvider dependencyProvider) {
        dependencyProvider.getResolver().inject(this);
    }

    /**
     * Switch Map ensures that API is called as soon as connection becomes available
     * @return live data of list of clients or null if there is an error
     */
    @Override
    public LiveData<List<Client>> getOnlineMerakiClients() {
        return Transformations.switchMap(connectivityRepo.isConnected(), connected -> {
            if (connected) {
                return merakiClientsRepository.getOnlineMerakiClients();
            }
            return new MutableLiveData<>();
        });
    }

    /**
     * Switch Map ensures that API is called as soon as connection becomes available
     * @return live data of list of clients or null if there is an error
     */
    @Override
    public LiveData<List<Client>> getOfflineMerakiClients() {
        return Transformations.switchMap(connectivityRepo.isConnected(), connected -> {
            if (connected) {
                return merakiClientsRepository.getOfflineMerakiClients();
            }
            return new MutableLiveData<>();
        });
    }
}
