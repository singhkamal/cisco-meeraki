package com.cisco.merakinetworks.middleware.model;

import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * <pre>
 * Array of Meraki Clients
 * check {@link Client}
 * </pre>
 */
public class MerakiClients {
    @SerializedName("clients")
    @Expose
    private List<Client> clients = null;

    public List<Client> getClients() {
        return clients;
    }

    public void setClients(List<Client> clients) {
        this.clients = clients;
    }

    public static JsonDeserializer<MerakiClients> getDeserializer() {
        return (json, typeOfT, context) -> {

            JsonObject root = json.getAsJsonObject();

            List<Client> merakiClientList = new ArrayList<>();

            JsonArray clients = root.get("clients").getAsJsonArray();

            for (int i = 0; i < clients.size(); i++) {
                JsonObject clientRoot = clients.get(i).getAsJsonObject();

                Client client = Client.fromJson(clientRoot);

                merakiClientList.add(client);
            }

            MerakiClients merakiClients = new MerakiClients();
            merakiClients.setClients(merakiClientList);
            return merakiClients;
        };
    }
}
