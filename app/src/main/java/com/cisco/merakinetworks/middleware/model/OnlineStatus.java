package com.cisco.merakinetworks.middleware.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * <pre>
 * Representing the online status
 *
 * "onlineStatus": {
 * 		"firstSeen": 1491370697,
 * 		"lastSeen": 1501894010,
 * 		"isOnline": true,
 * 		"connectedBy": "rsx"
 *  },
 *  </pre>
 */
public class OnlineStatus implements Parcelable {
    @SerializedName("firstSeen")
    @Expose
    private Integer firstSeen;
    @SerializedName("lastSeen")
    @Expose
    private Integer lastSeen;
    @SerializedName("isOnline")
    @Expose
    private Boolean isOnline;
    @SerializedName("connectedBy")
    @Expose
    private String connectedBy;

    public Integer getFirstSeen() {
        return firstSeen;
    }

    public void setFirstSeen(Integer firstSeen) {
        this.firstSeen = firstSeen;
    }

    public Integer getLastSeen() {
        return lastSeen;
    }

    public void setLastSeen(Integer lastSeen) {
        this.lastSeen = lastSeen;
    }

    public Boolean getIsOnline() {
        return isOnline;
    }

    public void setIsOnline(Boolean isOnline) {
        this.isOnline = isOnline;
    }

    public String getConnectedBy() {
        return connectedBy;
    }

    public void setConnectedBy(String connectedBy) {
        this.connectedBy = connectedBy;
    }

    protected OnlineStatus(Parcel in) {
        firstSeen = in.readByte() == 0x00 ? null : in.readInt();
        lastSeen = in.readByte() == 0x00 ? null : in.readInt();
        byte isOnlineVal = in.readByte();
        isOnline = isOnlineVal == 0x02 ? null : isOnlineVal != 0x00;
        connectedBy = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (firstSeen == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeInt(firstSeen);
        }
        if (lastSeen == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeInt(lastSeen);
        }
        if (isOnline == null) {
            dest.writeByte((byte) (0x02));
        } else {
            dest.writeByte((byte) (isOnline ? 0x01 : 0x00));
        }
        dest.writeString(connectedBy);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<OnlineStatus> CREATOR = new Parcelable.Creator<OnlineStatus>() {
        @Override
        public OnlineStatus createFromParcel(Parcel in) {
            return new OnlineStatus(in);
        }

        @Override
        public OnlineStatus[] newArray(int size) {
            return new OnlineStatus[size];
        }
    };
}