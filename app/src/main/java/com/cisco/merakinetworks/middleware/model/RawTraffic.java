package com.cisco.merakinetworks.middleware.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * <pre>
 * Representing raw traffic information
 *
 *   "rawTraffic": {
 *   		"applicationUsage": {@link ApplicationUsage},
 *   		"sent": 8519021.466497544,
 *   		"recv": 380646512.16828847,
 *   		"timeSeriesUsage": [[]]
 *    },
 *</pre>
 */
public class RawTraffic implements Parcelable {

    private Float sent;

    private Float recv;

    private List<List<Integer>> timeSeriesUsage = null;

    private List<ApplicationUsage> applicationUsage;

    public RawTraffic() {
    }

    protected RawTraffic(Parcel in) {
        sent = in.readByte() == 0x00 ? null : in.readFloat();
        recv = in.readByte() == 0x00 ? null : in.readFloat();

        if (in.readByte() == 0x01) {
            applicationUsage = new ArrayList<>();
            in.readList(applicationUsage, ApplicationUsage.class.getClassLoader());
        } else {
            applicationUsage = null;
        }

        if (in.readByte() != 0x00) {
            timeSeriesUsage = new ArrayList<>();
            int items = in.readInt();
            for (int i = 0; i < items; i++) {
                List<Integer> newList = new ArrayList<>();
                in.readList(newList, Integer.class.getClassLoader());
                timeSeriesUsage.add(newList);
            }
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (sent == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeFloat(sent);
        }
        if (recv == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeFloat(recv);
        }

        if (applicationUsage == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(applicationUsage);
        }

        if (timeSeriesUsage == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeInt(timeSeriesUsage.size());
            for (List<Integer> list : timeSeriesUsage) {
                dest.writeList(list);
            }
        }
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<RawTraffic> CREATOR = new Parcelable.Creator<RawTraffic>() {
        @Override
        public RawTraffic createFromParcel(Parcel in) {
            return new RawTraffic(in);
        }

        @Override
        public RawTraffic[] newArray(int size) {
            return new RawTraffic[size];
        }
    };

    public List<ApplicationUsage> getApplicationUsage() {
        return applicationUsage;
    }

    public void setApplicationUsage(List<ApplicationUsage> applicationUsage) {
        this.applicationUsage = applicationUsage;
    }

    public Float getSent() {
        return sent;
    }

    public void setSent(Float sent) {
        this.sent = sent;
    }

    public Float getRecv() {
        return recv;
    }

    public void setRecv(Float recv) {
        this.recv = recv;
    }

    public List<List<Integer>> getTimeSeriesUsage() {
        return timeSeriesUsage;
    }

    public void setTimeSeriesUsage(List<List<Integer>> timeSeriesUsage) {
        this.timeSeriesUsage = timeSeriesUsage;
    }

    /**
     * Custom JSON parsing for raw traffic object
     * {@link ApplicationUsage} object can have variable application usage entries. It is not possible
     * to create such POJO but relying on key pair entries makes it up for a more generic representation
     *
     * <br><br>
     *
     * Parsing logic is
     * <ul>
     *     <li>Extract the send data bytes value</li>
     *     <li>Extract the receive data bytes value</li>
     *     <li>Extract the timeSeriesUsage data bytes value.
     *     <ul>
     *         <li>First check how many sub list exists</li>
     *         <li>Parse each sub list and add the list to parent list</li>
     *     </ul>
     *     </li>
     * </ul>
     *
     * applicationUsage have variable entries, hence an ApplicationUsage list is built by reading
     * objects key pair. <br>
     *
     *  <ul>
     *      <li>Key - name of the application</li>
     *      <li>Value - usage of the application</li>
     *  </ul>
     *
     * @param jsonObject
     * @return
     */
    public static RawTraffic fromJson(JsonObject jsonObject) {
        RawTraffic rawTraffic = new RawTraffic();

        rawTraffic.setSent(jsonObject.get("sent").getAsFloat());

        rawTraffic.setRecv(jsonObject.get("recv").getAsFloat());

        List<List<Integer>> timeSeriesUsage = new ArrayList<>();

        JsonArray timeSeriesRoot = jsonObject.get("timeSeriesUsage").getAsJsonArray();

        for (int i = 0; i < timeSeriesRoot.size(); i++) {
            JsonArray timeSeriesItemRoot = timeSeriesRoot.get(i).getAsJsonArray();

            List<Integer> series = new ArrayList<>();

            for (int j = 0; j < timeSeriesItemRoot.size(); j++) {
                series.add(timeSeriesItemRoot.get(j).getAsInt());
            }

            if (!series.isEmpty()) {
                timeSeriesUsage.add(series);
            }
        }

        rawTraffic.setTimeSeriesUsage(timeSeriesUsage);

        List<ApplicationUsage> applicationUsage = new ArrayList<>();

        JsonObject applicationUsageRoot = jsonObject.get("applicationUsage").getAsJsonObject();
        for (String key : applicationUsageRoot.keySet()) {
            ApplicationUsage applicationUsageItem = new ApplicationUsage(key, applicationUsageRoot.get(key).getAsInt());
            applicationUsage.add(applicationUsageItem);
        }
        applicationUsage.sort(Collections.reverseOrder());
        rawTraffic.setApplicationUsage(applicationUsage);

        return rawTraffic;
    }
}
