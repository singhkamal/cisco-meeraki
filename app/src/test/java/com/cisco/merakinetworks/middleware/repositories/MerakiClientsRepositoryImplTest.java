package com.cisco.merakinetworks.middleware.repositories;

import androidx.arch.core.executor.testing.InstantTaskExecutorRule;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.cisco.merakinetworks.BuildConfig;
import com.cisco.merakinetworks.core.network.ConnectivityRepo;
import com.cisco.merakinetworks.middleware.model.Client;
import com.cisco.merakinetworks.middleware.model.MerakiClients;
import com.cisco.merakinetworks.middleware.net.MerakiApi;
import com.cisco.merakinetworks.utils.DummyResponse;
import com.google.gson.GsonBuilder;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class MerakiClientsRepositoryImplTest {

    @Mock
    ConnectivityRepo connectivityRepo;

    @Rule
    public InstantTaskExecutorRule instantExecutor = new InstantTaskExecutorRule();

    @Before
    public void setup() {

    }

    @After
    public void cleanup() {

    }

    /**
     * Test whether Meraki client Repo returns a list of online clients if user is offline
     * Expected behavior: Repo should not return any list. Null should be returned instead
     */
    @Test
    public void test_offline_getMerakiOnlineClients() {

        when(connectivityRepo.isConnected()).thenReturn(new MutableLiveData<>(false));

        MerakiClientsRepository merakiClientsRepository = new MerakiClientsRepositoryImpl(connectivityRepo, mock(MerakiApi.class));

        LiveData<List<Client>> result = merakiClientsRepository.getOnlineMerakiClients();

        assertNull(result.getValue());
    }

    /**
     * Test whether Meraki client Repo returns a list of offline clients if user is offline
     * Expected behavior: Repo should not return any list. Null should be returned instead
     */
    @Test
    public void test_offline_getMerakiOfflineClients() {

        when(connectivityRepo.isConnected()).thenReturn(new MutableLiveData<>(false));

        MerakiClientsRepository merakiClientsRepository = new MerakiClientsRepositoryImpl(connectivityRepo, mock(MerakiApi.class));

        LiveData<List<Client>> result = merakiClientsRepository.getOfflineMerakiClients();

        assertNull(result.getValue());
    }

    /**
     * Meraki client repo should return a list of clients if user is online
     *
     * Mockito is used to Mock various dependencies like ConnectivityRepo,
     * MerakiApi
     * @throws IOException
     */
    @Test
    public void test_online_getMerakiOnlineClients() {

        final CountDownLatch latch = new CountDownLatch(1);

        final boolean[] result = {false};

        // this ensures that connectivity repo is returning "true" during mocking
        when(connectivityRepo.isConnected()).thenReturn(new MutableLiveData<>(true));

        // to avoid actual network call, I am mocking the response here. This will ensure that
        // test case keeps running on devices/jenkins test automation servers
        Retrofit retrofit = new Retrofit.Builder()
                .client(new OkHttpClient.Builder()
                        .addInterceptor(DummyResponse.getResponseInterceptor())
                        .build())
                .addConverterFactory(GsonConverterFactory.create(new GsonBuilder()
                        .registerTypeAdapter(MerakiClients.class, MerakiClients.getDeserializer())
                        .create()))
                .baseUrl(BuildConfig.BASE_URL)
                .build();

        MerakiApi api  = retrofit.create(MerakiApi.class);

        MerakiClientsRepository merakiClientsRepository = new MerakiClientsRepositoryImpl(connectivityRepo, api);

        // We are not mocking the meraki repo, but it's dependencies.
        // having a countdown latch will help in exercising the entire loop of live data
        merakiClientsRepository.getOnlineMerakiClients().observeForever(clients -> {
            result[0] = clients.size() == 2;
            latch.countDown();
        });

        try {
            latch.await(5, TimeUnit.SECONDS);
        } catch (Exception e) {
            e.printStackTrace();
        }

        // check if repo has returned the expected answer
        assertTrue(result[0]);
    }
}