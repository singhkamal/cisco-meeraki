package com.cisco.merakinetworks.middleware.viewmodels;

import androidx.arch.core.executor.testing.InstantTaskExecutorRule;
import androidx.lifecycle.MutableLiveData;

import com.cisco.merakinetworks.DependencyProvider;
import com.cisco.merakinetworks.DependencyResolver;
import com.cisco.merakinetworks.core.network.ConnectivityRepo;
import com.cisco.merakinetworks.middleware.viewmodels.ConnectivityViewModel;
import com.cisco.merakinetworks.middleware.viewmodels.MerakiClientsViewModel;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

/**
 * To test the internals of the connectivity repo
 */
@RunWith(MockitoJUnitRunner.class)
public class ConnectivityViewModelTest {

    @Mock
    DependencyProvider dependencyProvider;

    @Mock
    ConnectivityRepo connectivityRepo;

    @Rule
    public InstantTaskExecutorRule instantExecutor = new InstantTaskExecutorRule();

    @Before
    public void setup() {

    }

    /**
     * Testing the scenario where repo reports data connectivity is available
     */
    @Test
    public void test_isConnected() {
        // This is quite a cool trick to provide Dagger dependency resolver to inject
        // mocked classes
        when(dependencyProvider.getResolver()).thenReturn(new DependencyResolver() {
            @Override
            public void inject(ConnectivityViewModel connectivityViewModel) {
                connectivityViewModel.connectivityRepo = connectivityRepo;
            }

            @Override
            public void inject(MerakiClientsViewModel merakiClientsViewModel) {

            }
        });

        when(connectivityRepo.isConnected()).thenReturn(new MutableLiveData<>(true));

        final CountDownLatch latch = new CountDownLatch(1);

        final boolean[] result = {false};

        ConnectivityViewModel viewModel = new ConnectivityViewModel(dependencyProvider);

        // I am only mocking dependencies of connectivity repo, but intent is to test full loop with
        // live data. Hence having a count down will enable this
        viewModel.isConnected().observeForever(aBoolean -> {
            result[0] = aBoolean;
            latch.countDown();
        });

        try {
            latch.await(1, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        assertTrue(result[0]);
    }

    /**
     * Testing the scenario where repo reports data connectivity is not available
     *
     */
    @Test
    public void test_isNotConnected() {
        // This is quite a cool trick to provide Dagger dependency resolver to inject
        // mocked classes
        when(dependencyProvider.getResolver()).thenReturn(new DependencyResolver() {
            @Override
            public void inject(ConnectivityViewModel connectivityViewModel) {
                connectivityViewModel.connectivityRepo = connectivityRepo;
            }

            @Override
            public void inject(MerakiClientsViewModel merakiClientsViewModel) {

            }
        });

        when(connectivityRepo.isConnected()).thenReturn(new MutableLiveData<>(false));

        final CountDownLatch latch = new CountDownLatch(1);

        final boolean[] result = {false};

        ConnectivityViewModel viewModel = new ConnectivityViewModel(dependencyProvider);

        // I am only mocking dependencies of connectivity repo, but intent is to test full loop with
        // live data. Hence having a count down will enable this
        viewModel.isConnected().observeForever(aBoolean -> {
            result[0] = aBoolean;
            latch.countDown();
        });

        try {
            latch.await(1, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        assertTrue(!result[0]);
    }
}